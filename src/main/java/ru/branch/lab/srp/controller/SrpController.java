package ru.branch.lab.srp.controller;

import org.springframework.web.bind.annotation.*;
import ru.branch.lab.srp.model.pub.*;
import ru.branch.lab.srp.service.SrpService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/srp")
public class SrpController {

    private final SrpService srpService;

    public SrpController(SrpService srpService) {
        this.srpService = srpService;
    }

    @PostMapping("/signup")
    public void signUp(@RequestBody SignUpRequest request) {
         srpService.signUp(request);
    }

    @PostMapping("/step1")
    public Step1PublicResponse step1(@RequestBody Step1PublicRequest request) {
        return srpService.step1(request);
    }

    @PostMapping("/step2")
    public Step2PublicResponse step2(@RequestBody Step2PublicRequest request) {
        return srpService.step2(request);
    }
}
