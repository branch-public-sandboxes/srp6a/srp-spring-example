package ru.branch.lab.srp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.branch.lab.srp.core.SRP6CryptoParams;
import ru.branch.lab.srp.core.SRP6Server;
import ru.branch.lab.srp.exception.SRP6Exception;
import ru.branch.lab.srp.model.*;
import ru.branch.lab.srp.model.pub.*;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SrpService {

    private static SRP6CryptoParams config = SRP6CryptoParams.getInstance(4096, "sha256");
    private static Map<String, UserEntity> storage = new HashMap<>();

    public void signUp(SignUpRequest request) {
        UserEntity user = new UserEntity();
        user.setLogin(request.getLogin());
        user.setSalt(new BigInteger(request.getSalt()));
        user.setVerifier(new BigInteger(request.getVerifier()));
        storage.put(request.getLogin(), user);
    }

    /**
     * Шаг 1:
     * <p>
     * Получить login и A
     * Вытащить по логину соль (s) и верификатор (v)
     * Вернуть клиенту соль (s) и B
     *
     * @return
     */
    public Step1PublicResponse step1(Step1PublicRequest request) {
        UserEntity userEntity = storage.get(request.getLogin());
        BigInteger salt = userEntity.getSalt();
        BigInteger verifier = userEntity.getVerifier();

        SRP6Server srv = new SRP6Server(config);
        Step1Request step1Request = new Step1Request(request.getLogin(), salt, verifier);
        Step1Response step1Response = srv.step1(step1Request);

        userEntity.setB(step1Response.getB());
        userEntity.setPrivateB(step1Response.getPrivateB());
        storage.put(userEntity.getLogin(), userEntity);

        return new Step1PublicResponse(step1Response.getB().toString(), salt.toString());
    }


    public Step2PublicResponse step2(Step2PublicRequest request) {
        UserEntity userEntity = storage.get(request.getLogin());

        Step2Request step2Request = new Step2Request(
                new BigInteger(request.getA()),
                new BigInteger(request.getM1()),
                userEntity.getB(),
                userEntity.getPrivateB(),
                userEntity.getVerifier()
        );

        SRP6Server srv = new SRP6Server(config);
        Step2Response step2Response = null;
        try {
            step2Response = srv.step2(step2Request);
        } catch (SRP6Exception e) {
            throw new RuntimeException(e);
        }
        // TODO: Сгенерировать JWT
        String JWT = "";
        return new Step2PublicResponse(step2Response.getM2().toString(), JWT);
    }
}
