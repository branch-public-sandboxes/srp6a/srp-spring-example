package ru.branch.lab.srp.model.pub;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@Setter
@Getter
public class SignUpRequest {
    private String login;
    private String salt;
    private String verifier;
}
