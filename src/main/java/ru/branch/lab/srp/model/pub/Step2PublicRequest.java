package ru.branch.lab.srp.model.pub;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@Setter
@Getter
public class Step2PublicRequest {
    private String login;
    private String A;
    private String M1;
}
