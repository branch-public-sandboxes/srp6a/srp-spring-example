package ru.branch.lab.srp.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@Setter
@Getter
public class UserEntity {
    private String login;
    private BigInteger salt;
    private BigInteger verifier;
    private BigInteger B;
    private BigInteger privateB;
}
