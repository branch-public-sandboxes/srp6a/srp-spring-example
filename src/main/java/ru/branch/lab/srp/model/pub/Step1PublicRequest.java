package ru.branch.lab.srp.model.pub;

import lombok.*;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Step1PublicRequest {
    private String login;
}
