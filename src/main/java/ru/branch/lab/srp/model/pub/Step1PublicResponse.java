package ru.branch.lab.srp.model.pub;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@Setter
@Getter
public class Step1PublicResponse {
    @JsonProperty("B")
    private String B;
    private String salt;
}
